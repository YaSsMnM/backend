<?php

namespace gardenBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * news
 *
 * @ORM\Table(name="news")
 * @ORM\Entity(repositoryClass="gardenBundle\Repository\newsRepository")
 */
class news
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="string", length=255)
     */
    private $message;

    /**
     *
     * @ORM\ManyToOne(targetEntity="garden")
     * @ORM\JoinColumn(name="garden",referencedColumnName="id")
     */
    private $garden;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return news
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return news
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return mixed
     */
    public function getGarden()
    {
        return $this->garden;
    }

    /**
     * @param mixed $garden
     */
    public function setGarden($garden)
    {
        $this->garden = $garden;
    }

}

