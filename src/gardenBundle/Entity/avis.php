<?php

namespace gardenBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * avis
 *
 * @ORM\Table(name="avis")
 * @ORM\Entity(repositoryClass="gardenBundle\Repository\avisRepository")
 */
class avis
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="commentaire", type="string", length=255)
     */
    private $commentaire;

    /**
     * @var string
     *
     * @ORM\Column(name="nomprop", type="string", length=255)
     */
    private $nomprop;

    /**
     *
     * @ORM\ManyToOne(targetEntity="garden")
     * @ORM\JoinColumn(name="garden",referencedColumnName="id")
     */
    private $garden;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set commentaire
     *
     * @param string $commentaire
     *
     * @return avis
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;
        return $this;
    }

    /**
     * Get commentaire
     *
     * @return string
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * @return string
     */
    public function getNomprop()
    {
        return $this->nomprop;
    }

    /**
     * @param string $nomprop
     */
    public function setNomprop($nomprop)
    {
        $this->nomprop = $nomprop;
    }

    /**
     * @return mixed
     */
    public function getGarden()
    {
        return $this->garden;
    }

    /**
     * @param mixed $garden
     */
    public function setGarden($garden)
    {
        $this->garden = $garden;
    }


}

