<?php

namespace gardenBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class gardenControllerTest extends WebTestCase
{
    public function testAdd()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'add_garden');
    }

    public function testUpdate()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'update_garden');
    }

    public function testDelete()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'delete_garden');
    }

    public function testGetll()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'getAll_garden');
    }

}
