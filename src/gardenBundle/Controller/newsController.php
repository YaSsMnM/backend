<?php

namespace gardenBundle\Controller;

use AgenceVoyageBundle\Entity\Compagnie;
use gardenBundle\Entity\garden;
use gardenBundle\Entity\news;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class newsController extends Controller
{
    /**
     * @Route("add_news")
     */
    public function addAction(Request $request)
    {
        $data = json_decode($request->getContent());
        $em = $this->getDoctrine()->getManager();
        $garden = $em->getRepository(garden::class)->findOneBy(array('id' => $data->garden));
        $news = new news();
        $news->setTitre($data->titre);
        $news->setMessage($data->message);
        $news->setGarden($garden);
        $em->persist($news);
        $em->flush();
        return new JsonResponse(
            [
                'status' => 'News ajouter avec succes',
            ],
            JsonResponse::HTTP_CREATED
        );
    }

    /**
     * @Route("update_news")
     */
    public function updateAction(Request $request, $id)
    {
        $news = $this->getDoctrine()->getRepository(news::class)->find($id);
        $data = $request->getContent();
        $newdata = $this->get('jms_serializer')->deserialize($data, 'gardenBundle\Entity\news', 'json');
        $news->setTitre(!$newdata->getTitre() ? $news->getTitre() : $newdata->getTitre());
        $news->setMessage(!$newdata->getMessage() ? $news->getMessage() : $newdata->getMessage());
        $em = $this->getDoctrine()->getManager();
        $em->persist($news);
        $em->flush();
        return new JsonResponse(
            [
                'status' => 'News modifier avec succes',
            ],
            JsonResponse::HTTP_OK
        );
    }

    /**
     * @Route("delete_news")
     */
    public function deleteAction($id)
    {
        $entities = $this->getDoctrine()
            ->getRepository(news::class)->findOneBy(array('id' => $id));
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($entities);
        $entityManager->flush();
        return new JsonResponse(
            [
                'status' => 'News supprimer avec succes',
            ],
            JsonResponse::HTTP_OK
        );
    }

    /**
     * @Route("getAll_news")
     */
    public function getAllAction()
    {
        $em = $this->getDoctrine()->getManager();
        $livres = $em->getRepository(news::class)->findAll();
        $data = $this->get('jms_serializer')->serialize($livres, 'json');
        $response = new Response($data);
        return $response;
    }

    /**
     * @Route("getById_news")
     */
    public function getByIdAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $news = $em->getRepository(news::class)->findBy(array('garden' => $id));
        $data = $this->get('jms_serializer')->serialize($news, 'json');
        $response = new Response($data);
        return $response;
    }

}
