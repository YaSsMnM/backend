<?php

namespace gardenBundle\Controller;

use AgenceVoyageBundle\Entity\Compagnie;
use gardenBundle\Entity\avis;
use gardenBundle\Entity\garden;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class avisController extends Controller
{
    /**
     * @Route("add_avis")
     */
    public function addAction(Request $request)
    {
        $data = json_decode($request->getContent());
        $em = $this->getDoctrine()->getManager();
        $garden = $em->getRepository(garden::class)->findOneBy(array('id' => $data->garden));
        $avis = new avis();
        $avis->setCommentaire($data->commentaire);
        $avis->setNomprop($data->nomprop);
        $avis->setGarden($garden);
        $em->persist($avis);
        $em->flush();
        return new JsonResponse(
            [
                'status' => 'Avis ajouter avec succes',
            ],
            JsonResponse::HTTP_CREATED
        );
    }

    /**
     * @Route("update_avis")
     */
    public function updateAction(Request $request, $id)
    {
        $avis = $this->getDoctrine()->getRepository(avis::class)->find($id);
        $data = $request->getContent();
        $newdata = $this->get('jms_serializer')->deserialize($data, 'gardenBundle\Entity\avis', 'json');
        var_dump($newdata);
        $avis->setCommentaire($newdata->getCommentaire() !== "" ? $newdata->getCommentaire() : $avis->getCommentaire());
        $em = $this->getDoctrine()->getManager();
        $em->persist($avis);
        $em->flush();
        return new JsonResponse(
            [
                'status' => 'Avis modifier avec succes',
            ],
            JsonResponse::HTTP_OK
        );
    }

    /**
     * @Route("delete_avis")
     */
    public function deleteAction($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entities = $entityManager->getRepository(avis::class)->findOneBy(array('id' => $id));
        $entityManager->remove($entities);
        $entityManager->flush();
        return new JsonResponse(
            [
                'status' => 'Avis supprimer avec succes',
            ],
            JsonResponse::HTTP_OK
        );
    }

    /**
     * @Route("getById_avis")
     */
    public function getByIdGardenAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $avis = $em->getRepository(avis::class)->findBy(array('garden' => $id));
        $data = $this->get('jms_serializer')->serialize($avis, 'json');
        $response = new Response($data);
        return $response;
    }

}
