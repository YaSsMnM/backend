<?php

namespace gardenBundle\Controller;

use gardenBundle\Entity\garden;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Doctrine\ORM\EntityManagerInterface;
use Gedmo\Sluggable\Util\Urlizer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class gardenController extends Controller
{

    /**
     * @Route("update_garden")
     */
    public function updateAction(Request $request, $id)
    {
        $data = $request->request->get('data');
        $garden = $this->getDoctrine()->getRepository(garden::class)->find($id);

        if (!empty($data)) {
            $array = json_decode($data, true);
            $garden->setDescription($array['description'] !== null ? $array['description'] : $garden->getDescription());
            if (array_key_exists('note', $array)) {
                $garden->setNote($array['note'] !== null ? $array['note'] : $garden->getNote());
            }
            if (array_key_exists('nbrec', $array)) {
                $garden->setNbrec($array['nbrec'] !== null ? $array['note'] : $garden->getNbrec());
            }
            $garden->setTarif($array['tarif'] !== null ? $array['tarif'] : $garden->getTarif());
            $garden->setHoraire($array['horaires'] !== null ? $array['horaires'] : $garden->getHoraire());
            $garden->setNbenfants($array['nbrEnfants'] !== null ? $array['nbrEnfants'] : $garden->getNbenfants());
            $garden->setCapacite($array['capacite'] !== null ? $array['capacite'] : $garden->getCapacite());
            //        $garden->setTags($newdata->getTags() !== null ? $newdata->getTags() : $garden->getTags());
        }
        $uploadedImage = $request->files->get('file');
        /**
         * @var UploadedFile $image
         */
        $image = $uploadedImage;
        $imageName = md5(uniqid()) . '.' . $image->guessExtension();
        $image->move($this->getParameter('image_directory'), $imageName);
        $garden->setPhoto($imageName);
        $em = $this->getDoctrine()->getManager();
        $em->persist($garden);
        $em->flush();
        return new JsonResponse(
            [
                'status' => 'Garden modifier avec succes',
            ],
            JsonResponse::HTTP_OK
        );
    }

    /**
     * @Route("delete_garden")
     */
    public function deleteAction($id)
    {
        $entities = $this->getDoctrine()
            ->getRepository(garden::class)->findOneBy(array('id' => $id));
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($entities);
        $entityManager->flush();
        return new JsonResponse(
            [
                'status' => 'Garden supprimer avec succes',
            ],
            JsonResponse::HTTP_OK
        );
    }

    /**
     * @Route("getAll_garden")
     */
    public function getAllAction()
    {
        $em = $this->getDoctrine()->getManager();
        $gardens = $em->getRepository(garden::class)->findAll();
        $data = $this->get('jms_serializer')->serialize($gardens, 'json');
        $response = new Response($data);
        return $response;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("add_garden")
     */
    public function addAction(Request $request)
    {
        $garden = $this->get('jms_serializer')->deserialize(($request->get('data')), 'gardenBundle\Entity\garden', 'json');
        $uploadedImage = $request->files->get('file');
        /**
         * @var UploadedFile $image
         */
        $image = $uploadedImage;
        $imageName = md5(uniqid()) . '.' . $image->guessExtension();
        $image->move($this->getParameter('image_directory'), $imageName);
        $garden->setPatente($imageName);
        $em = $this->getDoctrine()->getManager();
        $em->persist($garden);
        $em->flush();
        return new JsonResponse(
            [
                'status' => 'Garden ajouter avec succes',
            ],
            JsonResponse::HTTP_CREATED
        );
    }


    /**
     * @Route("images")
     * @return JsonResponse
     */
    public function imagesAction()
    {
        $images = $this->getDoctrine()->getRepository(garden::class)->findAll();
        $data = $this->get('jms_serializer')->serialize($images, 'json');
        $response = array(
            'message' => 'images loaded with sucesss',
            'result' => json_decode($data)
        );
        return new JsonResponse($response, 200);
    }


    /**
     * @param $id
     * @Route("image")
     * @return JsonResponse
     */
    public function imageAction($id)
    {
        $imageName = $this->getDoctrine()->getRepository(garden::class)->find($id);
        $response = array(
            'code' => 0,
            'message' => 'get image with success!',
            'errors' => null,
            'result' => $imageName->getPatente()
        );
        return new JsonResponse($response, 200);
    }


    /**
     * @Route("search")
     */
    public function searchAction(Request $request)
    {
        $data = $request->getContent();
        $garden = $this->get('jms_serializer')->deserialize($data, 'gardenBundle\Entity\garden', 'json');
        $repository = $this->getDoctrine()->getRepository(garden::class);
        $list = $repository->search($garden->getAdresse(), $garden->getNote(), $garden->getNbrec(), $garden->getTarif(), $garden->getNbenfants());
        $data = $this->get('jms_serializer')->serialize($list, 'json');
        $response = new Response($data);
        return $response;
    }
}
